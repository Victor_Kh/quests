import React from 'react';
import Form from '@/components/Form';

const Page = () => {
  return (
    <div className='w-full flex justify-center'>
      <Form />
    </div>
  );
};

export default Page;
