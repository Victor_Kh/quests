import Form from '@/components/Form';
import Modal from '@/components/common/Modal';

const Page = () => {
  return (
    <Modal>
      <Form />
    </Modal>
  );
};

export default Page;
