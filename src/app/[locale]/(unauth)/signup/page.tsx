import React from 'react';
import SignUpForm from '@/components/SignUpForm';

const Page = () => {
  return (
    <div className='w-full flex justify-center'>
      <SignUpForm />
    </div>
  );
};

export default Page;
