import React from 'react';
import SignInForm from '@/components/SignInForm';

const Page = () => {
  return (
    <div className='w-full flex justify-center'>
      <SignInForm />
    </div>
  );
};

export default Page;
