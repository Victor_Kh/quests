import React from 'react';

const Loading = () => {
  return (
    <div className='text-white text-[120px] pt-[32px] flex justify-center'>
      <span className='loader' />
    </div>
  );
};

export default Loading;
