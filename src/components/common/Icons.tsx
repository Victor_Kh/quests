import adventures from '../../../public/icons/adventures.svg';
import all_quests from '../../../public/icons/all_quests.svg';
import clock from '../../../public/icons/clock.svg';
import detective from '../../../public/icons/detective.svg';
import horror from '../../../public/icons/horrors.svg';
import instagram from '../../../public/icons/instagram.svg';
import mystic from '../../../public/icons/mystic.svg';
import person from '../../../public/icons/person.svg';
import puzzle from '../../../public/icons/puzzle.svg';
import sci_fi from '../../../public/icons/sci_fi.svg';
import twitter from '../../../public/icons/twitter.svg';
import youtube from '../../../public/icons/youtube.svg';

const Icons = {
  adventures,
  all_quests,
  clock,
  detective,
  horror,
  instagram,
  mystic,
  person,
  puzzle,
  'sci-fi': sci_fi,
  twitter,
  youtube,
};

export default Icons;
