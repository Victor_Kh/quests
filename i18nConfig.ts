const i18nConfig = {
  locales: ['en', 'uk'],
  defaultLocale: 'uk',
};

export default i18nConfig;
